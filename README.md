# djangocon2018-docker

This project was created to share Dockerfiles and other development assets
at DjangoCon US 2018.  The intent is to create a (set of) Dockerfiles to allow
easy setup of Django core test runs.

## Design notes

At DjangoCon US 2018, many people who showed up for the sprints and wanted to
contribute to sprinting on the Django core code had various problems getting
to the point where they could run the Django test suite.  This project aims to
be one possible solution to this problem, by providing a means for a Django
core developer to run the test suite without needing to modify their
local machine's environment or install (potentially invasive) database applications
or other packages (other than Docker itself.)

### Docker image steps

These are initial thoughts on the possible execution steps that the image might
take to accomplish its task.  These will certainly evolve over time as the
problem is better understood and lessons learned from experimentation.

#### Build-time steps

* Install system packages necessary to support building Python from source (via pyenv)
* Install system packages necessary to support client usage of targeted databases
* Install system packages necessary to build any Python test dependencies with C extensions
* Install initial version of pyenv
* Build targeted versions of python (3.4, 3.5, 3.6, 3.7?)
* Install virtualenvs for each targeted python version, and preinstall known
  requirements of django.  (The `py3.txt` requirements file has changed very,
  very rarely, so these should be safe to preinstall, to save the user the
  time and bandwidth.  If the versions of the dependencies change, the image
  can be rebuilt.)
* Set permissions for these virtualenvs so that they can be used by any user (i.e. 777?)

#### Runtime steps (entrypoint shell script)

* Entrypoint (shell) script runs as root
* The UID/GID of the user running the container is received from CLI parameters
  incoming to the entrypoint script
* A non-root user is created inside the container, with the UID and GID specified
  to be matching those of the external user.
* Symbolic links are made in the new user's home directory, pointing to the
  pre-installed virtualenvs.
* The desired execution profile (which python version, which database(s), etc.)
  is configured from CLI parameters.
* The entrypoint script changes the current directory to the mounted volume
  containing the external user's instance of the Django source code.
* The entrypoint script executes the `tests/runtests.py` module in the mounted
  volume, with the remaining CLI parameters received. This execution should be
  accomplished as the newly-created user, using `su -c`, or any
  better equivalent technique.

## Alpine Linux notes

* Alpine uses `musl` libc, which requires a [patch](https://git.alpinelinux.org/cgit/aports/tree/main/python3/musl-find_library.patch?h=3.8-stable)
  to enable python to build properly.
  The [distribution's own build script](https://git.alpinelinux.org/cgit/aports/tree/main/python3?h=3.8-stable) may be helpful in constructing our own
  method.
  Also see [this Alpine Linux bug report](https://bugs.alpinelinux.org/issues/5264).
* Packages required to build python from source on Alpine (beyond the fix above): `build-base libbz2 gdbm-dev xz-dev sqlite-dev libuuid libressl-dev readline-dev zlib-dev`.  Please add to the list if you find more that are necessary.
