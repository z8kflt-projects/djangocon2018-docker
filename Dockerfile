# Apparently a very small example
# https://blog.realkinetic.com/building-minimal-docker-containers-for-python-applications-37d0272c52f3
FROM python:3-slim


RUN apt-get update && apt-get install -y \
	build-essential libffi-dev libmemcached-dev zlib1g-dev

# RUN apk add --no-cache libffi-dev musl-dev 

# plylibmc dependencies
# RUN apk add --no-cache libmemcached-dev zlib-dev 

# Pillow dependencies
# RUN apk add --no-cache jpeg-dev zlib-dev

# libpng-dev jpeg-dev tiff-dev 

# ENV PYTHONUNBUFFERED 1

# Add git
# RUN apk update && apk upgrade && apk add git

# Django tests need to be run as a local user
RUN useradd -ms /bin/bash test_user
WORKDIR /home/test_user

# Add local version of django, currently assuming this is run in the 
# directory the local django checkout is in, maybe safer to go for root directory
USER test_user
RUN python3 -m venv /home/test_user/django-core
WORKDIR django-core
COPY django /home/test_user/django-core
# WORKDIR django
ENV PATH=/home/test_user/.local/bin:$PATH

# Get current version of django
# RUN git clone http://github.com/django/django

RUN pip install --user -e .
RUN pip install --user -r tests/requirements/py3.txt
WORKDIR tests
RUN ./runtests.py

# base, then add options for other Dockerfiles with sql
